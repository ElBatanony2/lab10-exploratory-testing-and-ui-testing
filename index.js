const { Builder, By, until } = require('selenium-webdriver');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        await driver.get('https://translate.google.com/');
        // Check page is loaded
        await driver.wait(until.titleIs('Google Translate'), 1000);
        // Get input text area
        const textarea = await driver.findElement(By.xpath('//*[@id="yDmH0d"]/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div[1]/div[2]/div[2]/c-wiz[1]/span/span/div/textarea'));
        // Add Привет as input for translation
        await textarea.sendKeys('Привет')
        // Wait until the translation element appears
        await driver.wait(until.elementLocated(By.xpath('//*[@id="ow158"]/div[1]/span[1]/span/span')))
        // Get the translation element
        const translationEl = await driver.findElement(By.xpath('//*[@id="ow158"]/div[1]/span[1]/span/span'));
        // Get the text (translation)
        const translation = await translationEl.getText()
        // Log the translation
        console.log(translation)
        // Check if the translation is correct
        if (translation == 'Hello')
            console.log('Test passed!')
        else console.log('Test failed')
    }
    catch (e) {
        console.log(e)
    }
    finally {
        await driver.quit();
    }
})();